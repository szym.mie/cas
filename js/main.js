const can = document.getElementById("main_canvas");
const ctx = can.getContext("2d");
const tim = document.getElementById("timer");
const mov = document.getElementById("moves");
const sel = document.getElementById("subdv");
const inp = document.getElementById("name");
const nin = document.getElementById("nameinput");
const stb = document.getElementById("scores");
const sli = document.getElementById("slider");


const IMAGES = ["/img/mountain.png", "/img/tiger.png", "/img/wheats.png"];
const DIFFICULTY = .0;

const _main = {T: 0, trsfrm: {d: 0, l: 0, t: []}, start: true, moves: 0, t: 0, selimg: 0, slibsy: false};

const timn = [...tim.children].filter(v => v.alt === "_");
const movn = [...mov.children].filter(v => v.alt === "_");


function initTiles(img_name, sub_divs) {

    const img = new Image();
    img.src = img_name;

    can.width = img.width;
    can.height = img.height;

    _main.trsfrm = {d: 0, l: 0, t: []};
    _main.start = true;
    _main.moves = -1;
    _main.t = 0;
    _main.dt = performance.now();
    _main.divs = sub_divs;
    _main.state = initState(sub_divs);
    moveupdt();
    resetbar();

    img.onload = async function () {
        const tile_width = this.width / sub_divs;
        _main.width = tile_width;
        _main.posit = [...genSubdivs(this.width, sub_divs)];
        await Promise.all(
            _main.posit.map(v => createImageBitmap(this, v.x, v.y, tile_width, tile_width))
        )
        .then(res => {_main.tiles = res;});
    };
}


function drawTiles() {
    _main.T++;
    try {
        ctx.clearRect(0, 0, can.width, can.height);
        [...zip(_main.state, _main.posit)]
            .filter(([v, _], i) => !_main.trsfrm.t.includes(i) && v)
            .map(([v, p], i) => {ctx.drawImage(
                _main.tiles[v],
                p.x,
                p.y)});
        try {
            const {x: ax, y: ay} = animTile();
            console.log(`a: ${ax}, ${ay}`)
            ctx.drawImage(_main.tiles[_main.state[_main.trsfrm.t[1]]], ax, ay);
        } catch {}
    } catch {}
    requestAnimationFrame(drawTiles);
}


function animTile() {
    if (_main.trsfrm.d <= 0) {
        _main.trsfrm.d = 0;
        _main.trsfrm.t = [];
    } else {
        const b = _main.trsfrm.t[0];
        const e = _main.trsfrm.t[1];
        console.log(`b: ${_main.posit[b].x}, ${_main.posit[b].y}\ne: ${_main.posit[e].x}, ${_main.posit[e].y}`);
        const q = Math.cos(_main.trsfrm.d--*Math.PI/(_main.trsfrm.l*2))**2;
        console.log(q);
        return lerp(_main.posit[b], _main.posit[e], q);
    }
}


function initSlider() {
    const slides = IMAGES.map(c => c);
    slides.unshift(IMAGES[IMAGES.length-1]);
    slides.push(IMAGES[0]);
    slides
        .map(v => {
            const e = document.createElement("div");
            const i = document.createElement("img");
            i.src = v;
            i.className = "sliderpic";
            e.appendChild(i);
            e.className = "sliderpic";
            sli.appendChild(e);
        });
    sli.scrollLeft = 100;
}


function slideLeft() {
    if (!_main.slibsy)
    {
        _main.slibsy = true;
        if (--_main.selimg < 0)
        {
            _main.selimg = IMAGES.length-1;
            sli.scrollTo((IMAGES.length+1)*100, 0);
        }
        loopDelay(
            () => {sli.scrollBy(-5, 0)},
            () => {_main.slibsy = false;}, 1, 20);
        shuffleTiles();
    }
}


function slideRight() {
    if (!_main.slibsy)
    {
        _main.slibsy = true;
        if (++_main.selimg > IMAGES.length-1)
        {
            _main.selimg = 0;
            sli.scrollTo(0, 0);
        }
        loopDelay(
            () => {sli.scrollBy(5, 0)},
            () => {_main.slibsy = false;}, 1, 20);
        shuffleTiles();
    }
}


function loopDelay(cb, en, dt, n) {
    let x = 0;
    const id = setInterval(function () {
        cb();
        if (++x == n)
        {
            en();
            clearInterval(id);
        }
    }, dt);
}


function *zip(...arr) {
    const len = Math.min(...arr.map(v => v.length));
    for (let i = 0; i < len; i++) yield arr.map(v => v[i])
}


function *genSubdivs(size, sub_divs) {
    const div = size / sub_divs;
    for (let i = 0; i < sub_divs; i++) {
        for (let j = 0; j < sub_divs; j++) {
            yield {x: i*div, y: j*div};
        }
    }
}


const initState = sub_divs => [...Array(sub_divs**2)].map((_, i) => i);
const findEmptyTile = () => _main.state.findIndex(v => !v);
const findMoveTiles = () => {
    const empty = findEmptyTile();
    const off = _main.divs;
    console.log([empty-1, empty+1, empty-off, empty+off]);
    return [empty-1, empty+1, empty-off, empty+off].filter(v => v >= 0 && v < _main.posit.length);
};
const inMoveAreas = coord => {
    const tiles = findMoveTiles();
    return tiles[
        tiles
            .map(v => _main.posit[v])
            .findIndex(v => inArea(coord, v))];
};

const inArea = ({x: x, y: y}, {x: tx, y: ty}) =>
(
    (x >= tx && x < tx + _main.width) &&
    (y >= ty && y < ty + _main.width)
);


function handleClick(ev) {
    const coord = {x: ev.offsetX, y: ev.offsetY};
    const m = inMoveAreas(coord);
    const e = findEmptyTile();
    if (m !== undefined) {
        _main.trsfrm = {d: 30, l: 30, t: [m, e]};
        _main.start = true;
        console.log(_main.trsfrm);
        moveTile(m);
        moveupdt();
    }
}


function moveTile(i) {
    const ei = findEmptyTile();
    const t = _main.state[ei];
    _main.state[ei] = _main.state[i];
    _main.state[i] = t;
}

function shuffleTiles(m=DIFFICULTY) {
    newGame();
    const tdiff = m * maxdiff();
    let sl = 0;
    let cn = 100;
    const len = _main.divs**2;
    while ((sl % 2 || sl < tdiff) && --cn) {
        console.log(sl);
        const i = Math.floor(Math.random()*len)
        moveTile(i);
        sl = solution();
    }
}

const solution = (s=_main.state) =>
    s.slice(0, -1).reduce((ag, cg, ig) =>
        ag + s
            .slice(ig)
            .filter(v => v < cg)
            .reduce((ae, ce) => ae+ce, 0)
    );

const timenumform = t => {
    const mse = Math.floor(t % 1000);
    const s = Math.floor(t / 1000);
    const sec = Math.floor(s % 60);
    const min = Math.floor(s / 60);
    setnum(timn, 0, min.toString().padStart(2, "0"));
    setnum(timn, 2, sec.toString().padStart(2, "0"));
    setnum(timn, 4, mse.toString().padStart(3, "0"));
}

const timestrform = t => {
    const mse = Math.floor(t % 1000);
    const s = Math.floor(t / 1000);
    const sec = Math.floor(s % 60);
    const min = Math.floor(s / 60);
    return `${min.toString().padStart(2,"0")}:${sec.toString().padStart(2,"0")}.${mse.toString().padStart(3,"0")}`;
}

const setnum = (d, b, s) => {
    d.slice(b,s.length+b).map((v, i) => v.src = num(s[i]));
}

const num = n => `/img/n${n}.svg`;

const timeupdt = () => {
    if (_main.start) _main.t = performance.now() - _main.dt;
    timenumform(_main.t);
    console.log("tupdate");
}

const moveupdt = () => {
    _main.moves++;
    setnum(movn, 0, _main.moves.toString().padStart(3, "0"));
    if (!solution()) solved();
    else resetbar();
}

const solved = () => {
    _main.start = false;
    [...tim.children].map(v => v.style.width = "80px");
    [...mov.children].map(v => v.style.width = "80px");
    showNameInput();
}

const resetbar = () => {
    [...tim.children].map(v => v.style.width = "50px");
    [...mov.children].map(v => v.style.width = "50px");
    hideNameInput();
}

const maxdiff = () =>
    solution([...Array(_main.divs**2)].map((_, i) => i).reverse());

const lerp = ({x: sx, y: sy}, {x: dx, y: dy}, q) =>
    ({x: sx*(1-q)+dx*q, y: sy*(1-q)+dy*q});

can.addEventListener("click", handleClick)

function newGame() {
    initTiles(IMAGES[_main.selimg], parseInt(sel.value));
    _main.start = true;
    readScore();
}

const ascii = t => 
    [...t].map(
        c => (c.charCodeAt() >= 0xff || c.charCodeAt() <= 0x1f) ? "" : c
        ).join("");

const censor = t =>
    [...t].map(
        c => {
            let r = "";
            switch(c) {
                case ";":
                    r = ":";
                    break;
                case "=":
                    r = "-";
                    break;
                case "%":
                    r = "#";
                    break;
                case "^":
                    r = "*";
                    break;
                case "[":
                    r = "{";
                    break;
                case "]":
                    r = "}";
                    break;
                default:
                    r = c;
            }
            return r;
        }).join("");

function addScore(name) {
    const time = Math.floor(_main.t);
    console.log(time);
    const scores = getScore().map(v => ({n: v.split("%")[0], t: parseInt(v.split("%")[1])}));
    console.log(scores);
    const spind = scores.findIndex(v => v.t >= time);
    console.log(spind);
    if (spind < 0)
        scores.push({n: name, t: time});
    else {
        console.log(!scores.some(v => (v.n == name && v.t == time)));
        if (!scores.some(v => (v.n == name && v.t == time))) {
            const stt = scores.slice(0, spind);
            const end = scores.slice(spind);
            console.log(stt, end);
            scores.splice(0);
            console.log(stt, end);
            Object.assign(scores, scores.concat(stt, {n: name, t: time}, end));
        }
    }
    console.log(scores);
    document.cookie = `scores=[${scores.slice(0, 10).reduce((a, c, i) => a.concat(`${i ?"^":""}${ascii(censor(c.n))}%${c.t}`), "")}];`;
}

function readScore() {
    const scores = getScore().map(v => ({n: v.split("%")[0], t: parseInt(v.split("%")[1])}));
    stb.innerHTML = scores.reduce((a, c, i) => a.concat(i < 10 ? `<li>${timestrform(c.t)}   →   ${c.n}</li>` : ""), "");
}

function getScore() {
    try {
        const scores = document.cookie.match(/scores=\[\S*\]/g)[0];
        return scores.split("=")[1].slice(1, -1).split("^");
    } catch { return []; }
}

function showNameInput() {
    inp.style.visibility = "visible";
}

function hideNameInput() {
    inp.style.visibility = "hidden";
}

function submitScore() {
    addScore(nin.value);
    hideNameInput();
    readScore();
    shuffleTiles();
}

setInterval(timeupdt, 20);
shuffleTiles();
initSlider();
drawTiles();
if (!can.width) initTiles(IMAGES[_main.selimg], parseInt(sel.value));
